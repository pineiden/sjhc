from django.contrib import admin

# Register your models here.
from .models import CentroEstudios, Postulante
# Register your models here.

class PostulanteAdmin(admin.ModelAdmin):
	list_display = ("profesion","nombre","pub_date","email")
admin.site.register(Postulante,PostulanteAdmin)
admin.site.register(CentroEstudios)