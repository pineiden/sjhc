from django.forms import ModelForm
from .models import Postulante
from django.core.validators import MinLengthValidator, MaxLengthValidator,MinValueValidator,MaxValueValidator
from captcha.fields import ReCaptchaField

class PostulanteForm(ModelForm):
	captcha = ReCaptchaField()

	def __init__(self, *args, **kwargs):
		super(PostulanteForm, self).__init__(*args, **kwargs)
		self.fields["nombre"].min_length = 3
		self.fields["nombre"].validators.append(MinLengthValidator)

		self.fields["telefono"].validators.append(MinValueValidator(100))

		self.fields["telefono"].validators.append(MaxValueValidator(999999999))

		self.fields["centro_estudios"].min_length = 4
		self.fields["centro_estudios"].validators.append(MinValueValidator)

		self.fields["presentacion"].min_length = 100
		self.fields["presentacion"].validators.append(MinValueValidator)

		self.fields["presentacion"].max_length = 500
		self.fields["presentacion"].validators.append(MaxValueValidator)


	def clean(self):
		cleaned_data = super(PostulanteForm, self).clean()
				
	class Meta:
		model = Postulante
		fields = [
			'nombre',
			'profesion',
			'centro_estudios',
			'curriculo',
			'email',
			'telefono',
			'presentacion'
			]