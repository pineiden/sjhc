from django.db import models
import datetime
# Create your models here.
from django.utils.translation import ugettext_lazy as _
from .validators import validate_file_extension


def get_upload_cv_file_name(instance, filename):
	date = datetime.datetime.today().date()
	year = date.year
	month =date.month
	return "curriculos/%s/%s/%s" %(year,month,filename)

class CentroEstudios(models.Model):
	nombre = models.CharField(max_length=30)
	webpage = models.CharField(max_length=200)
	def __str__(self):
		return self.nombre

	@property
	def CentroEstudio(self):
		return self.nombre

	class Meta:
		verbose_name = _("Centro de Estudios")
		verbose_name_plural = _("Centros de Estudios")
		ordering = ("nombre","webpage")

class Postulante(models.Model):
	nombre = models.CharField(max_length=100)
	profesion = models.CharField(max_length=60)
	centro_estudios = models.ForeignKey(CentroEstudios)
	curriculo = models.FileField(upload_to=get_upload_cv_file_name,validators=[validate_file_extension])
	pub_date = models.DateTimeField(auto_now=True)
	email = models.EmailField()
	telefono = models.IntegerField()
	presentacion = models.TextField()


	def __str__(self):
		return self.nombre

	@property
	def ContactForm(self):
		return self.nombre

	class Meta:
		verbose_name = _("Postulante")
		verbose_name_plural = _("Postulantes")
		ordering = ("profesion","nombre")

