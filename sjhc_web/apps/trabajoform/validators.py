import os
def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf', '.doc', '.docx', '.odt', '.html']
    if not ext in valid_extensions:
        raise ValidationError(u'Extensión de archivo no corresponde.')