from django.forms import modelformset_factory
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from .form import PostulanteForm
from .models import get_upload_cv_file_name
# Create your views here.
from django.contrib import messages

from django.conf import settings
from django.core.mail import send_mail
from sjhc_web.utils import send_html_mail

# Create your views here.
def formulario_postulante(request):
	#post = get_object_or_404(ContactForm)
	if request.method == "POST":
		form = PostulanteForm(request.POST, request.FILES)
		if form.is_valid():
			get_upload_cv_file_name(form,request.FILES['curriculo'])
			post = form.save(commit=True)
			post.save()
			centro_estudios=post.centro_estudios.nombre
			try:
				curriculo=post.curriculo.url
			except Exception as e:
				print('%s (%s)' % (e.message, type(e)))
			nombre= post.nombre
			profesion=post.profesion
			telefono=post.telefono
			email=post.email
			subject=post.nombre+", "+post.profesion +" desea postular a trabajar con SJHC"


			mensaje=post.presentacion
			message="Estimadas Patricia y Paulina:\n "+nombre+"\n Email: "+email+"\n fono: "+str(telefono)+"\n Currículo"\
						" descargable en: http://www.sanjosehomecare.cl"+curriculo+"\nHa enviado su postulación " \
						"para trabajar con SJHC.\nSe presenta con el siguiente mensaje:\n" \
						"\n"+mensaje
			send_html_mail(subject,
						   message,
						   ['pcordova1615@gmail.com','paulina_molinafeliu@hotmail.com'],
						   settings.EMAIL_HOST_USER)
			messages.success(request, 'Formulario de postulante enviado. ¡Muchas gracias!')
			#return redirect('formulario_enviado')#cambiar a pagina de enviado
	else:
		form = PostulanteForm()
	return render(request, 'postulante.html', {'form': form})
