--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO dev;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO dev;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO dev;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO dev;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO dev;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO dev;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO dev;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO dev;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO dev;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO dev;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO dev;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO dev;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: contactform_contact; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE contactform_contact (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    pub_date timestamp with time zone NOT NULL,
    telefono integer NOT NULL,
    email character varying(254) NOT NULL,
    asunto character varying(120) NOT NULL,
    mensaje text NOT NULL
);


ALTER TABLE contactform_contact OWNER TO dev;

--
-- Name: contactform_contact_servicio; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE contactform_contact_servicio (
    id integer NOT NULL,
    contact_id integer NOT NULL,
    pagina_id integer NOT NULL
);


ALTER TABLE contactform_contact_servicio OWNER TO dev;

--
-- Name: contactform_contactform_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE contactform_contactform_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contactform_contactform_id_seq OWNER TO dev;

--
-- Name: contactform_contactform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE contactform_contactform_id_seq OWNED BY contactform_contact.id;


--
-- Name: contactform_contactform_servicio_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE contactform_contactform_servicio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contactform_contactform_servicio_id_seq OWNER TO dev;

--
-- Name: contactform_contactform_servicio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE contactform_contactform_servicio_id_seq OWNED BY contactform_contact_servicio.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO dev;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO dev;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO dev;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO dev;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO dev;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO dev;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO dev;

--
-- Name: pagina_clasificacion; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE pagina_clasificacion (
    id integer NOT NULL,
    tipo character varying(20) NOT NULL,
    descripccion text NOT NULL,
    fecha_publicacion timestamp with time zone NOT NULL,
    slug_tipo character varying(50) NOT NULL
);


ALTER TABLE pagina_clasificacion OWNER TO dev;

--
-- Name: pagina_clasificacion_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE pagina_clasificacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pagina_clasificacion_id_seq OWNER TO dev;

--
-- Name: pagina_clasificacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE pagina_clasificacion_id_seq OWNED BY pagina_clasificacion.id;


--
-- Name: pagina_pagina; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE pagina_pagina (
    id integer NOT NULL,
    titulo character varying(100) NOT NULL,
    fecha_publicacion timestamp with time zone NOT NULL,
    cuerpo text NOT NULL,
    clasificacion_id integer,
    imagen character varying(100) NOT NULL,
    resumen text NOT NULL,
    slug_titulo character varying(50) NOT NULL
);


ALTER TABLE pagina_pagina OWNER TO dev;

--
-- Name: pagina_pagina_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE pagina_pagina_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pagina_pagina_id_seq OWNER TO dev;

--
-- Name: pagina_pagina_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE pagina_pagina_id_seq OWNED BY pagina_pagina.id;


--
-- Name: trabajoform_centroestudios; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE trabajoform_centroestudios (
    id integer NOT NULL,
    nombre character varying(30) NOT NULL,
    webpage character varying(200) NOT NULL
);


ALTER TABLE trabajoform_centroestudios OWNER TO dev;

--
-- Name: trabajoform_centroestudios_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE trabajoform_centroestudios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE trabajoform_centroestudios_id_seq OWNER TO dev;

--
-- Name: trabajoform_centroestudios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE trabajoform_centroestudios_id_seq OWNED BY trabajoform_centroestudios.id;


--
-- Name: trabajoform_postulante; Type: TABLE; Schema: public; Owner: dev; Tablespace: 
--

CREATE TABLE trabajoform_postulante (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    profesion character varying(60) NOT NULL,
    curriculo character varying(100),
    pub_date timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL,
    telefono integer NOT NULL,
    presentacion text NOT NULL,
    centro_estudios_id integer NOT NULL
);


ALTER TABLE trabajoform_postulante OWNER TO dev;

--
-- Name: trabajoform_postulante_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE trabajoform_postulante_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE trabajoform_postulante_id_seq OWNER TO dev;

--
-- Name: trabajoform_postulante_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE trabajoform_postulante_id_seq OWNED BY trabajoform_postulante.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY contactform_contact ALTER COLUMN id SET DEFAULT nextval('contactform_contactform_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY contactform_contact_servicio ALTER COLUMN id SET DEFAULT nextval('contactform_contactform_servicio_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY pagina_clasificacion ALTER COLUMN id SET DEFAULT nextval('pagina_clasificacion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY pagina_pagina ALTER COLUMN id SET DEFAULT nextval('pagina_pagina_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY trabajoform_centroestudios ALTER COLUMN id SET DEFAULT nextval('trabajoform_centroestudios_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY trabajoform_postulante ALTER COLUMN id SET DEFAULT nextval('trabajoform_postulante_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add Página	7	add_pagina
20	Can change Página	7	change_pagina
21	Can delete Página	7	delete_pagina
22	Can add Clasificación	8	add_clasificacion
23	Can change Clasificación	8	change_clasificacion
24	Can delete Clasificación	8	delete_clasificacion
28	Can add Formulario de Contacto	10	add_contact
29	Can change Formulario de Contacto	10	change_contact
30	Can delete Formulario de Contacto	10	delete_contact
31	Can add Centro de Estudios	11	add_centroestudios
32	Can change Centro de Estudios	11	change_centroestudios
33	Can delete Centro de Estudios	11	delete_centroestudios
34	Can add Postulante	12	add_postulante
35	Can change Postulante	12	change_postulante
36	Can delete Postulante	12	delete_postulante
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('auth_permission_id_seq', 36, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
2	pbkdf2_sha256$24000$AXpDkyte6JYI$jumqCWMqL7gCjEeWkXuIvyCzXHY9Yo4hlLSFBK8wihw=	2016-01-05 08:00:35.120586-03	f	Patricia	Patricia	Cordova Arellano	pcordova1615@gmail.com	t	t	2016-01-05 07:50:46-03
1	pbkdf2_sha256$24000$F5URGx3xy6dS$DvlYCCTQF/nylVXBNIO3phzeictFVdQ9Tor4PFkQb1o=	2016-02-02 00:25:16.015558-03	t	admin			dahalpi@gmail.com	t	t	2016-01-02 14:42:09.668617-03
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('auth_user_id_seq', 2, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
1	2	13
2	2	14
3	2	15
4	2	19
5	2	20
6	2	21
7	2	22
8	2	23
9	2	24
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 9, true);


--
-- Data for Name: contactform_contact; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY contactform_contact (id, nombre, pub_date, telefono, email, asunto, mensaje) FROM stdin;
1	David Pineda	2016-01-10 13:45:57.757429-03	82142267	dahalpi@gmail.com	Necesito durante una semana	Hola.\r\n\r\nSoy David Pineda, estoy probando formulario de contacto
9	David Pineda	2016-01-10 17:41:22.132336-03	82142267	dahalpi@gmail.com	Saludo test	sadsad\r\nsadsa\r\nd\r\nsa\r\ndxzcxzcxzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzcassssssssssssss\r\nasdsadasd\r\nasd\r\nsa\r\nd\r\nsa\r\nd\r\nsad\r\nsadaaaaaaaaaaaaaaaa\r\nwqe\r\nqwe\r\nwq\r\ne\r\nwq\r\ne\r\nwq\r\neqw
10	David Pineda	2016-01-10 18:01:27.169388-03	82142267	dahalpi@gmail.com	Saludo test	sadsad\r\nsadsa\r\nd\r\nsa\r\ndxzcxzcxzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzcassssssssssssss\r\nasdsadasd\r\nasd\r\nsa\r\nd\r\nsa\r\nd\r\nsad\r\nsadaaaaaaaaaaaaaaaa\r\nwqe\r\nqwe\r\nwq\r\ne\r\nwq\r\ne\r\nwq\r\neqw
11	David Pineda	2016-01-10 18:01:31.813931-03	82142267	dahalpi@gmail.com	Saludo test	sadsad\r\nsadsa\r\nd\r\nsa\r\ndxzcxzcxzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzcassssssssssssss\r\nasdsadasd\r\nasd\r\nsa\r\nd\r\nsa\r\nd\r\nsad\r\nsadaaaaaaaaaaaaaaaa\r\nwqe\r\nqwe\r\nwq\r\ne\r\nwq\r\ne\r\nwq\r\neqw
12	Felipe Castillo	2016-01-10 18:02:31.526005-03	78864536	dpineda@gmail.com	Sasasaasxc sadsa	dsadsad\r\nxc\r\n\r\nas\r\ndsa\r\nd\r\nas\r\nd\r\nasdddddddddddddddddddd2312\r\n321333333333333333333333333333333333333333333333333354\r\n45\r\n33333333333333335yety\r\nf\r\nsdf\r\nsdf\r\nsdf\r\nsd\r\nf\r\ns\r\ndf\r\nasssssssssssssssda\r\n\r\ndasd
\.


--
-- Data for Name: contactform_contact_servicio; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY contactform_contact_servicio (id, contact_id, pagina_id) FROM stdin;
1	1	6
2	1	7
3	9	6
4	9	7
5	10	6
6	10	7
7	11	6
8	11	7
9	12	9
10	12	10
\.


--
-- Name: contactform_contactform_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('contactform_contactform_id_seq', 12, true);


--
-- Name: contactform_contactform_servicio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('contactform_contactform_servicio_id_seq', 10, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2016-01-02 14:43:18.841248-03	1	Test	1	Añadido.	8	1
2	2016-01-02 14:59:44.431507-03	1	Hola Mundo	1	Añadido.	7	1
3	2016-01-02 15:43:10.500871-03	1	Hola Mundo	2	Modificado/a imagen.	7	1
4	2016-01-02 15:43:16.110137-03	1	Hola Mundo	2	Modificado/a imagen.	7	1
5	2016-01-02 15:47:14.279752-03	1	Hola Mundo	2	Modificado/a imagen.	7	1
6	2016-01-02 15:47:20.06777-03	1	Hola Mundo	2	Modificado/a imagen.	7	1
7	2016-01-02 15:56:46.482464-03	1	Hola Mundo	2	Modificado/a imagen.	7	1
8	2016-01-02 15:56:55.43744-03	1	Hola Mundo	2	Modificado/a imagen.	7	1
9	2016-01-02 16:31:42.522894-03	2	Prueba 2	1	Añadido.	7	1
10	2016-01-02 16:47:20.192702-03	3	Prueba 3 Scarlet	1	Añadido.	7	1
11	2016-01-02 18:25:59.955557-03	3	Prueba 3 Scarlet	2	Modificado/a cuerpo.	7	1
12	2016-01-02 18:27:05.881373-03	3	Prueba 3 Scarlet	2	Modificado/a cuerpo.	7	1
13	2016-01-02 18:27:54.920996-03	3	Prueba 3 Scarlet	2	Modificado/a cuerpo.	7	1
14	2016-01-02 21:09:09.904758-03	3	Prueba 3 Scarlet	2	Modificado/a cuerpo.	7	1
15	2016-01-02 21:11:06.574941-03	3	Prueba 3 Scarlet	2	No ha cambiado ningún campo.	7	1
16	2016-01-05 07:50:46.649946-03	2	Patricia	1	Añadido.	4	1
17	2016-01-05 07:52:29.938294-03	2	Patricia	2	Modificado/a first_name, last_name, email, is_staff y user_permissions.	4	1
18	2016-01-05 07:54:28.556284-03	2	Patricia	2	Modificado/a user_permissions.	4	1
19	2016-01-05 08:15:59.145862-03	2	principal	1	Añadido.	8	2
20	2016-01-05 08:19:59.500521-03	4	Bienvenido a San José Home Care	1	Añadido.	7	2
21	2016-01-05 08:23:37.234156-03	4	Bienvenido a San José Home Care	2	Modificado/a cuerpo.	7	2
22	2016-01-09 19:34:35.863555-03	4	Bienvenido a San José Home Care	2	Modificado/a cuerpo.	7	2
23	2016-01-09 19:40:41.088249-03	3	nosotros	1	Añadido.	8	2
24	2016-01-09 19:40:51.152117-03	5	¿Quiénes somos?	1	Añadido.	7	2
25	2016-01-09 19:48:28.035429-03	4	Servicios	1	Añadido.	8	2
26	2016-01-09 19:48:32.402676-03	6	Procedimientos de Enfermería	1	Añadido.	7	2
27	2016-01-09 19:58:17.224063-03	6	Procedimientos de Enfermería	2	Modificado/a cuerpo.	7	2
28	2016-01-09 20:06:28.234197-03	7	Enfermeras(os) Universitarias(os)	1	Añadido.	7	2
29	2016-01-09 20:23:01.478369-03	8	Técnicos de enfermería	1	Añadido.	7	2
30	2016-01-09 20:29:15.836828-03	9	Kinesiología	1	Añadido.	7	2
31	2016-01-09 20:34:57.672814-03	10	Arriendo de equipos médicos	1	Añadido.	7	2
32	2016-01-09 20:35:22.609587-03	3	Nosotros	2	Modificado/a tipo.	8	2
33	2016-01-09 20:35:29.54962-03	2	Principal	2	Modificado/a tipo.	8	2
34	2016-01-09 20:37:24.016753-03	5	Misión	1	Añadido.	8	2
35	2016-01-09 20:37:25.371244-03	11	Misión	1	Añadido.	7	2
36	2016-01-10 13:39:58.221475-03	1	Preuba form	1	Añadido.	\N	1
37	2016-01-10 13:40:46.394037-03	1	Prueba form	2	Modificado/a nombre.	\N	1
38	2016-01-10 13:40:59.117378-03	1	David Pineda	2	Modificado/a nombre.	\N	1
39	2016-01-10 13:45:57.76173-03	1	David Pineda	2	Modificado/a servicio.	\N	1
40	2016-01-10 17:41:37.558766-03	2	David Pineda	3		10	1
41	2016-01-10 17:41:37.621563-03	3	David Pineda	3		10	1
42	2016-01-10 17:41:37.632778-03	4	ol	3		10	1
43	2016-01-10 17:41:37.643945-03	5	ol	3		10	1
44	2016-01-10 17:41:37.654915-03	6	ol	3		10	1
45	2016-01-10 17:41:37.666407-03	7	David Pineda	3		10	1
46	2016-01-10 17:41:37.677558-03	8	David Pineda	3		10	1
47	2016-01-10 19:23:40.40218-03	1	Universidad de Chile	1	Añadido.	11	1
48	2016-01-10 19:35:00.604283-03	1	David Pineda	1	Añadido.	12	1
49	2016-01-10 19:35:35.177895-03	1	David Pineda	2	Modificado/a curriculo.	12	1
50	2016-01-10 19:50:52.89532-03	3	Gabriel Martins	2	Modificado/a curriculo.	12	1
51	2016-01-10 19:51:00.49025-03	2	Gabriel Martins	2	Modificado/a curriculo.	12	1
52	2016-01-19 21:47:24.373643-03	2	Prueba 2	2	Modificado/a cuerpo.	7	1
53	2016-02-02 08:45:24.268049-03	4	Bienvenido a San José Home Care	2	Modificado/a cuerpo.	7	1
54	2016-02-04 22:39:14.77814-03	11	Misión	2	Modificado/a imagen y resumen.	7	1
55	2016-02-04 22:39:26.480852-03	5	¿Quiénes somos?	2	Modificado/a imagen.	7	1
56	2016-02-04 22:39:35.74724-03	6	Procedimientos de Enfermería	2	Modificado/a imagen.	7	1
57	2016-02-04 22:39:44.182946-03	8	Técnicos de enfermería	2	Modificado/a imagen.	7	1
58	2016-02-04 22:39:51.555637-03	9	Kinesiología	2	Modificado/a imagen.	7	1
59	2016-02-04 22:40:08.338388-03	7	Enfermeras(os) Universitarias(os)	2	Modificado/a imagen.	7	1
60	2016-02-04 22:45:40.700001-03	10	Arriendo de equipos médicos	2	Modificado/a imagen.	7	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 60, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	pagina	pagina
8	pagina	clasificacion
10	contactform	contact
11	trabajoform	centroestudios
12	trabajoform	postulante
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('django_content_type_id_seq', 12, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-01-02 14:00:56.750068-03
2	auth	0001_initial	2016-01-02 14:00:57.587358-03
3	admin	0001_initial	2016-01-02 14:00:57.809878-03
4	admin	0002_logentry_remove_auto_add	2016-01-02 14:00:57.876472-03
5	contenttypes	0002_remove_content_type_name	2016-01-02 14:00:57.920987-03
6	auth	0002_alter_permission_name_max_length	2016-01-02 14:00:57.943013-03
7	auth	0003_alter_user_email_max_length	2016-01-02 14:00:57.976503-03
8	auth	0004_alter_user_username_opts	2016-01-02 14:00:57.995014-03
9	auth	0005_alter_user_last_login_null	2016-01-02 14:00:58.009752-03
10	auth	0006_require_contenttypes_0002	2016-01-02 14:00:58.021083-03
11	auth	0007_alter_validators_add_error_messages	2016-01-02 14:00:58.040279-03
12	pagina	0001_initial	2016-01-02 14:00:58.265933-03
13	pagina	0002_auto_20151226_1955	2016-01-02 14:00:58.388688-03
14	pagina	0003_auto_20151226_2032	2016-01-02 14:00:58.401884-03
15	pagina	0004_auto_20151226_2111	2016-01-02 14:00:58.657017-03
16	pagina	0005_auto_20151227_1543	2016-01-02 14:00:58.755316-03
17	pagina	0006_auto_20151227_1611	2016-01-02 14:00:58.855679-03
18	pagina	0007_auto_20151227_1615	2016-01-02 14:00:58.877886-03
19	pagina	0008_auto_20151227_1629	2016-01-02 14:00:59.089374-03
20	pagina	0009_auto_20151227_1631	2016-01-02 14:00:59.155694-03
21	pagina	0010_auto_20151227_1744	2016-01-02 14:00:59.178109-03
22	sessions	0001_initial	2016-01-02 14:00:59.367176-03
23	pagina	0011_auto_20160102_1944	2016-01-02 16:44:28.684466-03
24	pagina	0012_auto_20160102_2140	2016-01-02 18:40:31.907527-03
25	pagina	0013_auto_20160102_2147	2016-01-02 18:48:00.660399-03
26	contactform	0001_initial	2016-01-10 00:40:01.590971-03
27	contactform	0002_auto_20160110_1659	2016-01-10 13:59:41.890882-03
28	trabajoform	0001_initial	2016-01-10 19:17:18.866752-03
29	trabajoform	0002_auto_20160110_2223	2016-01-10 19:23:09.557921-03
30	trabajoform	0003_auto_20160206_1530	2016-02-06 12:30:54.301749-03
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('django_migrations_id_seq', 30, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
t1cu2nb2iq49j6abfh4u0oqyp1bn9jln	MDBhZjMyNzBmMDhiOTcwYjY2NDU5MWEwZjYwOTBkZmJhY2FkNjE2OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzY0ZDFlMjZlMjM1Njk2ODMyYmZjM2IyYmQ0MmNhNGM4NGIxYWEzMiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2016-01-16 14:42:20.192675-03
49zwpl1bd2tur8wbshthl1qwwxf2gngp	MWFlMmI2MTIxZDJkMjU4YzZiNmMyNGEyMTg3MmJlYWUyYWY4ZTYzMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJjNjRkMWUyNmUyMzU2OTY4MzJiZmMzYjJiZDQyY2E0Yzg0YjFhYTMyIn0=	2016-01-24 00:40:29.579792-03
pxkoqxhuce4fls00sgzrbiobdt2alj44	YzRkN2I2YjUzZTA5NzE0Zjk1MWRhN2IyNGZiNDJmNzI5Mzc1NjM1MTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzY0ZDFlMjZlMjM1Njk2ODMyYmZjM2IyYmQ0MmNhNGM4NGIxYWEzMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-02-02 21:41:16.413193-03
01xe17pbbwpurij9ljmwu4me896e9kqw	Mzc3Y2M4ZTE2YjgyN2ZjOTc0Y2U0ODBkODRmYmQ2N2M5ZjE2MzU5ZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjNjRkMWUyNmUyMzU2OTY4MzJiZmMzYjJiZDQyY2E0Yzg0YjFhYTMyIn0=	2016-02-02 23:17:26.636486-03
nsnkfd9zs8oj004rgjgh656g3o6yge6y	MDBhZjMyNzBmMDhiOTcwYjY2NDU5MWEwZjYwOTBkZmJhY2FkNjE2OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzY0ZDFlMjZlMjM1Njk2ODMyYmZjM2IyYmQ0MmNhNGM4NGIxYWEzMiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2016-02-16 00:24:03.97043-03
kt9yuk6jdjsswqrcdth7fvqyqhjhnf3b	MDBhZjMyNzBmMDhiOTcwYjY2NDU5MWEwZjYwOTBkZmJhY2FkNjE2OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzY0ZDFlMjZlMjM1Njk2ODMyYmZjM2IyYmQ0MmNhNGM4NGIxYWEzMiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2016-02-16 00:25:16.026854-03
\.


--
-- Data for Name: pagina_clasificacion; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY pagina_clasificacion (id, tipo, descripccion, fecha_publicacion, slug_tipo) FROM stdin;
1	Test	Pruebas de sitioweb	2016-01-02 14:43:18.840453-03	test
4	Servicios	Contenidos referentes a los servicios que provee la empresa	2016-01-09 19:48:28.034968-03	servicios
3	Nosotros	Para contenidos referenciados a los que componen la empresa	2016-01-09 20:35:22.608876-03	nosotros
2	Principal	Contenidos a desplegarse en la página principal	2016-01-09 20:35:29.548121-03	principal
5	Misión	Misión de la empresa	2016-01-09 20:37:24.016219-03	mision
\.


--
-- Name: pagina_clasificacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('pagina_clasificacion_id_seq', 5, true);


--
-- Data for Name: pagina_pagina; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY pagina_pagina (id, titulo, fecha_publicacion, cuerpo, clasificacion_id, imagen, resumen, slug_titulo) FROM stdin;
8	Técnicos de enfermería	2016-02-04 22:39:44.181501-03	<p>Profesional t&eacute;cnico responsable del cuidado permanente del paciente.</p>\r\n\r\n<p>Su labor consiste en entregar cuidados de enfermer&iacute;a como aseo y confort, administraci&oacute;n de medicamentos, acompa&ntilde;ando al paciente las 24 horas del d&iacute;a si lo requiere.</p>	4	imagenes/paginas/tecnicos-de-enfermeria/tec.en_enfermeria.jpg	Personas que realizan labores de cuidado permanente	tecnicos-de-enfermeria
1	Hola Mundo	2016-01-02 15:56:55.435974-03	Esta es unaprueba de concepto del modelo	1	imagenes/paginas/hola-mundo/emma_watson-gprofile.jpg	Prueba con subida de imagen	hola-mundo
2	Prueba 2	2016-01-19 21:47:24.371872-03	<p>Se prueba subida de im&aacute;genes en ckeditor&nbsp;<img alt="" height="261" src="http://eselcine.com/wp-content/uploads/2013/05/nicole-kidman-imagen.jpg" width="349" /> NO siempre ser&aacute; tan bella</p>	1	imagenes/paginas/prueba-2/nicole.jpg	Nueva prueba	prueba-2
4	Bienvenido a San José Home Care	2016-02-02 08:45:24.233078-03	<p><strong>San Jos&eacute; Home Care Spa</strong> le da la bienvenida a descubrir un grupo de<em><strong> Profesionales de la Salud</strong></em> que pretende mejorar la<strong><em> Calidad de Vida</em></strong> de nuestros pacientes, otorgando cuidados en la comodidad de su hogar.</p>\r\n\r\n<p>Contamos con personal altamente calificado, con insumos y equipos m&eacute;dicos para prestar el mejor servicio m&eacute;dico a nuestros pacientes que prefieren nuestros cuidados.</p>	2	imagenes/paginas/bienvenido-a-san-jose-home-care/HomeCare.png	Texto de bienvenida a San Jose Home Care	bienvenido-a-san-jose-home-care
11	Misión	2016-02-04 22:39:14.74465-03	<p>Nuestro objetivo es mejorar la calidad de vida del paciente, otorgando un trato personalizado en la comodidad de su hogar.</p>\r\n\r\n<p>&nbsp;</p>	5	imagenes/paginas/mision/mision.jpg	Servicio de calidad al paciente	mision
3	Prueba 3 Scarlet	2016-01-02 21:11:06.574041-03	<p>No se si se podr&aacute; considerar a Scarlet como una sucesora en talento a Nicole, tampoco en elegancia. Sin embargo ambas guardan una belleza que arrastrar&aacute; la ovaci&oacute;n de los admiradores en la pr&oacute;xima pel&iacute;cula en que ambas aparecen y dan todo para exponer su belleza en la pantalla gigante.</p>\r\n\r\n<p><img alt="" src="/media/ck_uploads/admin/2016/01/02/nicole.jpg" style="width: 214px; height: 317px;" /></p>	1	imagenes/paginas/prueba-3-scarlet/scarlet.jpg	Una chica bella llamada Scarlet y otra llamada Nicole	prueba-3-scarlet
5	¿Quiénes somos?	2016-02-04 22:39:26.479724-03	<p>Somos un <strong>Equipo Humano de Profesionales de la Salud</strong> que surge por la necesidad de otorgar <strong>cuidados integrales</strong> al paciente que requiere una atenci&oacute;n de <strong>calidad en su domicilio</strong>.</p>	3	imagenes/paginas/quienes-somos/quienes_somos.jpg	Sobre nosotros	quienes-somos
6	Procedimientos de Enfermería	2016-02-04 22:39:35.746185-03	<p>Nuestra empresa realiza los siguientes servicios de enfermer&iacute;a:</p>\r\n\r\n<ul>\r\n\t<li>Tratamientos endovenosos</li>\r\n\t<li>Nutrici&oacute;n enteral</li>\r\n\t<li>Curaci&oacute;n avanzada de heridas</li>\r\n\t<li>Manejo de ostom&iacute;as\r\n\t<ul>\r\n\t\t<li>Traqueostom&iacute;a</li>\r\n\t\t<li>Gastrostom&iacute;a</li>\r\n\t\t<li>Ileostom&iacute;a</li>\r\n\t\t<li>Colostom&iacute;a</li>\r\n\t</ul>\r\n\t</li>\r\n\t<li>Nebulizaciones</li>\r\n\t<li>Oxigenoterapias</li>\r\n\t<li>Manejo de sondas y drenajes</li>\r\n</ul>	4	imagenes/paginas/procedimientos-de-enfermeria/procedimientos_de_enfermeria.jpg	Servicios de enfermería ofrecidos	procedimientos-de-enfermeria
9	Kinesiología	2016-02-04 22:39:51.554564-03	<p>Este servicio facilita un profesiona especialista en la atenci&oacute;n al paciente.</p>\r\n\r\n<p>Tiene como objetivo apoyar en el control de s&iacute;ntomas y fomentar la funcionalidad e independencia en las actividades de la vida diaria, de acuerdo a la tolerancia y condiciones de cada paciente.</p>\r\n\r\n<p>La kinesiolog&iacute;a puede ser:</p>\r\n\r\n<ul>\r\n\t<li>motora</li>\r\n\t<li>respiratoria y/o</li>\r\n\t<li>drenaje linf&aacute;tico</li>\r\n</ul>\r\n\r\n<p>seg&uacute;n las indicaciones de su m&eacute;dico.</p>	4	imagenes/paginas/kinesiologia/kinesiologa.jpg	Servicios de kinesiología	kinesiologia
7	Enfermeras(os) Universitarias(os)	2016-02-04 22:40:08.337309-03	<p>Profesional responsable de los cuidados del paciente.</p>\r\n\r\n<p>En cada visita debe realizar una evaluaci&oacute;n de s&iacute;ntomas, administrar tratamientos y otros procedimientos relacionados con el equipo m&eacute;dico. Adem&aacute;s realiza la educaci&oacute;n al paciente y/o familia dirigida al autocuidado.</p>	4	imagenes/paginas/enfermerasos-universitariasos/enfermeras_universitarias.jpe	Atención de enfermeras(os) a domicilio	enfermerasos-universitariasos
10	Arriendo de equipos médicos	2016-02-04 22:45:40.698746-03	<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;">Disponemos de una amplia variedad de equipos m&eacute;dicos que podemos facilitar a m&oacute;dicos precios para lograr la mejor atenci&oacute;n posible al paciente:</p>\r\n\r\n<ul dir="ltr">\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Bombas de infusi&oacute;n continua</li>\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Motores de aspiraci&oacute;n y nebulizaci&oacute;n</li>\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Equipos de administraci&oacute;n de ox&iacute;geno, concentradores de ox&iacute;geno y balones</li>\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Monitores de oximetr&iacute;a de pulso</li>\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Camas cl&iacute;nicas manuales y el&eacute;ctricas (incluye servicio de ropa de cama)</li>\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Colchones antiescaras de presi&oacute;n alternante</li>\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Equipos de Hemoglucotest (control de glicemia)</li>\r\n\t<li style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">Sillas de rueda, andadores, etc.</li>\r\n</ul>	4	imagenes/paginas/arriendo-de-equipos-medicos/equipos_medicos.jpeg	Equipos médicos disponibles	arriendo-de-equipos-medicos
\.


--
-- Name: pagina_pagina_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('pagina_pagina_id_seq', 11, true);


--
-- Data for Name: trabajoform_centroestudios; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY trabajoform_centroestudios (id, nombre, webpage) FROM stdin;
1	Universidad de Chile	www.uchile.cl
\.


--
-- Name: trabajoform_centroestudios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('trabajoform_centroestudios_id_seq', 1, true);


--
-- Data for Name: trabajoform_postulante; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY trabajoform_postulante (id, nombre, profesion, curriculo, pub_date, email, telefono, presentacion, centro_estudios_id) FROM stdin;
1	David Pineda	Ingeniero Civil Electricista	curriculos/2016/1/curriculo_david_pineda_fondart.docx	2016-01-10 19:35:35.17605-03	dahalpi@gmail.com	82142267	asdasdasdasdasd\r\nas\r\ndas\r\ndsadas\r\nd\r\nasdddddddddddddddddddddddddddddddddddddd\r\ncccccccccccccccccccccqweeeeeeeeeeeeeee\r\nq322222222222222222	1
4	Gabriel Martins	Ingeniero Civil Electricista		2016-01-10 19:49:10.040855-03	dahalpi@gmail.com	34324234	adsadasda\r\nsd\r\nsa\r\ndas\r\nd\r\nas\r\nd\r\nsacxxxxxxxxxxxxxxxxxxxxxxxxxxxz\r\nasdsaddddddddddddddddddd\r\n432efdsdfs	1
5	Gabriel Martins	Ingeniero Civil Electricista	curriculos/2016/1/curriculo_david_pineda_ingeniero.pdf	2016-01-10 19:49:15.807574-03	dahalpi@gmail.com	34324234	adsadasda\r\nsd\r\nsa\r\ndas\r\nd\r\nas\r\nd\r\nsacxxxxxxxxxxxxxxxxxxxxxxxxxxxz\r\nasdsaddddddddddddddddddd\r\n432efdsdfs	1
3	Gabriel Martins	Ingeniero Civil Electricista	curriculos/2016/1/curriculo_david_pineda_ingeniero.docx	2016-01-10 19:50:52.894396-03	dahalpi@gmail.com	34324234	adsadasda\r\nsd\r\nsa\r\ndas\r\nd\r\nas\r\nd\r\nsacxxxxxxxxxxxxxxxxxxxxxxxxxxxz\r\nasdsaddddddddddddddddddd\r\n432efdsdfs	1
2	Gabriel Martins	Ingeniero Civil Electricista	curriculos/2016/1/curriculo_david_pineda_ingeniero_XHrPyOs.pdf	2016-01-10 19:51:00.489325-03	dahalpi@gmail.com	34324234	adsadasda\r\nsd\r\nsa\r\ndas\r\nd\r\nas\r\nd\r\nsacxxxxxxxxxxxxxxxxxxxxxxxxxxxz\r\nasdsaddddddddddddddddddd\r\n432efdsdfs	1
6	Gabriel Martins	Ingeniero Civil Electricista	curriculos/2016/1/curriculo_david_pineda_ingeniero_94NCf2w.pdf	2016-01-10 19:54:09.944143-03	dahalpi@gmail.com	34324234	adsadasda\r\nsd\r\nsa\r\ndas\r\nd\r\nas\r\nd\r\nsacxxxxxxxxxxxxxxxxxxxxxxxxxxxz\r\nasdsaddddddddddddddddddd\r\n432efdsdfs	1
7	Gabriel Martins	Ingeniero Civil Electricista	curriculos/2016/1/curriculo_david_pineda_ingeniero_CcXJovn.pdf	2016-01-10 19:54:14.033193-03	dahalpi@gmail.com	34324234	adsadasda\r\nsd\r\nsa\r\ndas\r\nd\r\nas\r\nd\r\nsacxxxxxxxxxxxxxxxxxxxxxxxxxxxz\r\nasdsaddddddddddddddddddd\r\n432efdsdfs	1
8	Jose Martins	Ingeniero Civil Electricista	curriculos/2016/1/curriculo_david_pineda_ingeniero_p48cr9Y.docx	2016-01-10 19:54:30.111217-03	dahalpi@gmail.com	34324234	adsadasda\r\nsd\r\nsa\r\ndas\r\nd\r\nas\r\nd\r\nsacxxxxxxxxxxxxxxxxxxxxxxxxxxxz\r\nasdsaddddddddddddddddddd\r\n432efdsdfs	1
\.


--
-- Name: trabajoform_postulante_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('trabajoform_postulante_id_seq', 8, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: contactform_contactform_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY contactform_contact
    ADD CONSTRAINT contactform_contactform_pkey PRIMARY KEY (id);


--
-- Name: contactform_contactform_servicio_contactform_id_34d583d2_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_contactform_servicio_contactform_id_34d583d2_uniq UNIQUE (contact_id, pagina_id);


--
-- Name: contactform_contactform_servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_contactform_servicio_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: pagina_clasificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY pagina_clasificacion
    ADD CONSTRAINT pagina_clasificacion_pkey PRIMARY KEY (id);


--
-- Name: pagina_clasificacion_tipo_8364cf48_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY pagina_clasificacion
    ADD CONSTRAINT pagina_clasificacion_tipo_8364cf48_uniq UNIQUE (tipo);


--
-- Name: pagina_pagina_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY pagina_pagina
    ADD CONSTRAINT pagina_pagina_pkey PRIMARY KEY (id);


--
-- Name: pagina_pagina_slug_titulo_ff1b2c47_uniq; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY pagina_pagina
    ADD CONSTRAINT pagina_pagina_slug_titulo_ff1b2c47_uniq UNIQUE (slug_titulo);


--
-- Name: trabajoform_centroestudios_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY trabajoform_centroestudios
    ADD CONSTRAINT trabajoform_centroestudios_pkey PRIMARY KEY (id);


--
-- Name: trabajoform_postulante_pkey; Type: CONSTRAINT; Schema: public; Owner: dev; Tablespace: 
--

ALTER TABLE ONLY trabajoform_postulante
    ADD CONSTRAINT trabajoform_postulante_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: contactform_contactform_servicio_02f97d6d; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX contactform_contactform_servicio_02f97d6d ON contactform_contact_servicio USING btree (pagina_id);


--
-- Name: contactform_contactform_servicio_9493ea24; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX contactform_contactform_servicio_9493ea24 ON contactform_contact_servicio USING btree (contact_id);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: pagina_clasificacion_5fe7cbf2; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX pagina_clasificacion_5fe7cbf2 ON pagina_clasificacion USING btree (slug_tipo);


--
-- Name: pagina_pagina_b5e06b77; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX pagina_pagina_b5e06b77 ON pagina_pagina USING btree (slug_titulo);


--
-- Name: pagina_pagina_e2b7bb94; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX pagina_pagina_e2b7bb94 ON pagina_pagina USING btree (clasificacion_id);


--
-- Name: trabajoform_postulante_8935f5dd; Type: INDEX; Schema: public; Owner: dev; Tablespace: 
--

CREATE INDEX trabajoform_postulante_8935f5dd ON trabajoform_postulante USING btree (centro_estudios_id);


--
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactform_conta_contact_id_ae984b54_fk_contactform_contact_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_conta_contact_id_ae984b54_fk_contactform_contact_id FOREIGN KEY (contact_id) REFERENCES contactform_contact(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactform_contactform__pagina_id_eb43e01e_fk_pagina_pagina_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_contactform__pagina_id_eb43e01e_fk_pagina_pagina_id FOREIGN KEY (pagina_id) REFERENCES pagina_pagina(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pagina_pag_clasificacion_id_8daed73f_fk_pagina_clasificacion_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY pagina_pagina
    ADD CONSTRAINT pagina_pag_clasificacion_id_8daed73f_fk_pagina_clasificacion_id FOREIGN KEY (clasificacion_id) REFERENCES pagina_clasificacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tr_centro_estudios_id_e35153af_fk_trabajoform_centroestudios_id; Type: FK CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY trabajoform_postulante
    ADD CONSTRAINT tr_centro_estudios_id_e35153af_fk_trabajoform_centroestudios_id FOREIGN KEY (centro_estudios_id) REFERENCES trabajoform_centroestudios(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

