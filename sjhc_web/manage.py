#!/usr/bin/env python
import os
import sys
from django.conf import settings

print(settings.MEDIA_URL)
print(settings.MEDIA_ROOT)
print(settings.STATIC_URL)
print(settings.STATIC_ROOT)
print("\n")

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sjhc_web.settings.development")

    from django.core.management import execute_from_command_line

    if 'livereload' in sys.argv:
        from django.core.wsgi import get_wsgi_application
        from livereload import Server
        application = get_wsgi_application()
        server = Server(application)

        # Add your watch
        # server.watch('path/to/file', 'your command')
        server.serve(port=8080, host='localhost')
    else:
        execute_from_command_line(sys.argv)
