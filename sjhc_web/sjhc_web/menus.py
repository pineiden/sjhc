from menu import Menu, MenuItem

# Add two items to our main menu
Menu.add_item("principal", MenuItem("Inicio",
                               reverse("home"),
                               weight=0,
                               icon="tools"))

Menu.add_item("principal", MenuItem("Misión",
                               "pagina/mision",
                               weight=20,
                               icon="report"))