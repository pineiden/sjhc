"""sjhc_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from .views import home_files
from apps.pagina import urls
from apps.contactform import urls
from apps.trabajoform import urls
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.views.static import serve

from django.contrib import admin
admin.autodiscover()
from django.views.static import serve
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^(?P<filename>(robots.txt)|(humans.txt))$',home_files, name='home-files'),
    #url(r'^google79a757e135d8a2b3.html$', TemplateView.as_view(template_name="google79a757e135d8a2b3.html")),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^', include('apps.pagina.urls')),
    url(r'^', include('apps.contactform.urls')),
    url(r'^', include('apps.trabajoform.urls')),
]

urlpatterns += [
	url(r'^static/(?P<path>.*)$', serve,
	{'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
	url(r'^media/(?P<path>.*)$', serve,
	{'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
]